package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

func init() {

}

func downloadUrlToFile(url string, fileName string) error {
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	size, err := io.Copy(file, resp.Body)
	defer file.Close()
	fmt.Printf("Downloaded a file %s (size %d)\n", fileName, size)
	return nil
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func warnCheck(err error) bool {
	if err != nil {
		log.Printf("WARNING: %s\n", err)
	}
	return err != nil
}

func main() {
	resp, err := http.Get(os.Args[1])
	check(err)
	defer resp.Body.Close()

	download_dir := fmt.Sprintf("fonts-%d", time.Now().Unix())
	download_fonts_dir := filepath.Join(download_dir, "files")
	font_css := filepath.Join(download_dir, "font.css")

	err = os.Mkdir(download_dir, 0755)
	check(err)
	err = os.Mkdir(download_fonts_dir, 0755)
	check(err)
	f, err := os.Create(font_css)
	check(err)
	defer f.Close()

	scanner := bufio.NewScanner(resp.Body)

	for scanner.Scan() {
		line := scanner.Text()
		r, _ := regexp.Compile("^(.*url\\()([^\\)]*)(\\).*)$")
		l := r.FindStringSubmatch(line)
		if len(l) > 0 {
			css_url := l[2]
			// Build fileName from fullPath
			fileURL, err := url.Parse(css_url)
			if !warnCheck(err) {
				path := fileURL.Path
				segments := strings.Split(path, "/")
				fileName := segments[len(segments)-1]

				err = downloadUrlToFile(css_url, filepath.Join(download_fonts_dir, fileName))
				warnCheck(err)
				f.WriteString(l[1] + filepath.Join("files", fileName) + l[3])

			}
		} else {
			f.WriteString(line)
		}
		f.WriteString("\n")
	}

}
