# apis-dl
Small program which can be used to create offline version of googleapis.com fonts

```html
 <style>
@import url('https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&display=swap');
</style>
```

## Usage
```shell
go run . 'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&display=swap'
```
new folder with name fonts-$UNIX_TIMESTAMP will be created containing font files and styles
